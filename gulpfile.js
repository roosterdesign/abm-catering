'use strict';
 var gulp = require('gulp'),
  less = require('gulp-less'),
  path = require('path'),
  cssmin = require('gulp-cssmin'),
  combineMq = require('gulp-combine-mq'),
  plumber = require('gulp-plumber'),
  notify = require("gulp-notify"),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename');

var onError = function (err) {  
  notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Error: <%= error.message %>",
    sound:    ""
  })(err);
  this.emit('end');
};

gulp.task('less', function() {
  return gulp.src('./wp-content/themes/abm-catering/css/src/styles.less')
    .pipe(plumber({errorHandler: onError}))
    .pipe(less({ paths: [ path.join(__dirname, 'less', 'includes') ] }))
    .pipe(combineMq())
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./wp-content/themes/abm-catering/css/dist/'));
});

gulp.task('scripts', function() {
  gulp.src([
      './wp-content/themes/abm-catering/js/src/plugins/jquery.easing.js',
      './wp-content/themes/abm-catering/js/src/plugins/jquery.owl-carousel.js',
      './wp-content/themes/abm-catering/js/src/plugins/jquery.matchHeight.js',
      './wp-content/themes/abm-catering/js/src/main.js'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./wp-content/themes/abm-catering/js/dist/'))
});

gulp.task('watch', function () {
  gulp.watch(['./wp-content/themes/abm-catering/css/src/**/*.less'], ['less']);
  gulp.watch('./wp-content/themes/abm-catering/js/src/main.js', ['scripts']);
});

gulp.task('default', ['less', 'scripts', 'watch']);