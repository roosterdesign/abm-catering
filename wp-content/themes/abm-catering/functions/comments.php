<?php 


// This is the new comment markup 
function html5_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <article <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
      
      <header class="comment-author vcard">
         <?php echo get_avatar($comment,$size='60',$default='<path_to_url>' ); ?>

         <?php printf(__('<span class="someonesays"><cite class="fn">%s</cite> <span class="says">says:</span></span>'), get_comment_author_link()) ?>
            <div class="comment-meta commentmetadata"><time datetime="<?php the_time('Y-m-d') ?>" pubdate><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a></time><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
      </header>
      
      <div class="clear"></div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.') ?></em>
      <?php endif; ?>

      <?php comment_text() ?>

      <div class="reply"> <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?> </div>
<?php
        }

// Changes the trailing </li> into a trailing </article>
function close_comment() {?>
  </article>
<?php
}


?>