<?php 

/*--- SORT OUT THE REQUIRED TYPES OF IMAGE SIZES ---*/ 
 
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'blog_thumbnail', '430', '270', true);
	add_image_size( 'blog_hero', '2560', '515', true);
}


function remove_default_image_sizes( $sizes) {
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['medium_large']);
    //unset( $sizes['large']);
    unset( $sizes['full']);
    return $sizes;
}

add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
add_filter('jpeg_quality', function($arg){return 50;});
add_filter( 'wp_editor_set_quality', function($arg){return 50;} );


// Default Gravatar
add_filter( 'avatar_defaults', 'wpb_new_gravatar' );
	function wpb_new_gravatar ($avatar_defaults) {
	$myavatar = "/wp-content/uploads/2017/09/avatar.gif";
	$avatar_defaults[$myavatar] = "Default Gravatar";
	return $avatar_defaults;
}

?>