<?php 
function setup_theme_admin_menus() {  
  add_menu_page('Theme settings', 'Site Settings', 'manage_categories', 'aub_theme_settings');
  add_submenu_page('aub_theme_settings', 'Front Page Elements', 'Front Page', 'manage_categories', 'aub_theme_settings', 'theme_front_page_settings');
}  
  
function theme_front_page_settings() { 

  $features1_title = get_option("features1_title");
  $features1_body = get_option("features1_body");
  $features1_image = get_option("features1_image");

  $features2_title = get_option("features2_title");
  $features2_body = get_option("features2_body");
  $features2_image = get_option("features2_image");

  $features3_title = get_option("features3_title");
  $features3_body = get_option("features3_body");
  $features3_image = get_option("features3_image");

  $features4_title = get_option("features4_title");
  $features4_body = get_option("features4_body");
  $features4_image = get_option("features4_image");

  $features5_title = get_option("features5_title");
  $features5_body = get_option("features5_body");
  $features5_image = get_option("features5_image");

  $features6_title = get_option("features6_title");
  $features6_body = get_option("features6_body");
  $features6_image = get_option("features6_image");

  if (isset($_POST["update_settings"])) :   
    $features1_title = esc_attr($_POST["features1_title"]);
    update_option("features1_title", $features1_title);
    $features1_body = esc_attr($_POST["features1_body"]);
    update_option("features1_body", $features1_body);
    $features1_image = esc_attr($_POST["features1_image"]);
    update_option("features1_image", $features1_image);

    $features2_title = esc_attr($_POST["features2_title"]);
    update_option("features2_title", $features2_title);
    $features2_body = esc_attr($_POST["features2_body"]);
    update_option("features2_body", $features2_body);
    $features2_image = esc_attr($_POST["features2_image"]);
    update_option("features2_image", $features2_image);

    $features3_title = esc_attr($_POST["features3_title"]);
    update_option("features3_title", $features3_title);
    $features3_body = esc_attr($_POST["features3_body"]);
    update_option("features3_body", $features3_body);
    $features3_image = esc_attr($_POST["features3_image"]);
    update_option("features3_image", $features3_image);

    $features4_title = esc_attr($_POST["features4_title"]);
    update_option("features4_title", $features4_title);
    $features4_body = esc_attr($_POST["features4_body"]);
    update_option("features4_body", $features4_body);
    $features4_image = esc_attr($_POST["features4_image"]);
    update_option("features4_image", $features4_image);

    $features5_title = esc_attr($_POST["features5_title"]);
    update_option("features5_title", $features5_title);
    $features5_body = esc_attr($_POST["features5_body"]);
    update_option("features5_body", $features5_body);
    $features5_image = esc_attr($_POST["features5_image"]);
    update_option("features5_image", $features5_image);

    $features6_title = esc_attr($_POST["features6_title"]);
    update_option("features6_title", $features6_title);
    $features6_body = esc_attr($_POST["features6_body"]);
    update_option("features6_body", $features6_body);
    $features6_image = esc_attr($_POST["features6_image"]);
    update_option("features6_image", $features6_image);
  ?>  

  
    <div id="message" class="updated"><p>Settings saved</p></div>  
  
  <?php endif; ?>  

  <div class="wrap">  
    <?php screen_icon('themes'); ?> <h2>General Website Settings</h2>  
    <form method="POST" action="">  
      <input type="hidden" name="update_settings" value="Y" />  
      <table class="form-table">  

        <tr><th scope="row" colspan="2"><h3 style="margin-bottom:0;">Features</h3></th></tr>

        <tr valign="top">  
          <th scope="row"><label for="features1_title">Feature 1 Title</label></th>  
          <td><input type="text" name="features1_title" value="<?php echo $features1_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features1_body">Feature 1 Body</label></th>  
          <td><input type="text" name="features1_body" value="<?php echo $features1_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features1_image">Feature 1 Image</label></th>  
          <td><input type="text" name="features1_image" value="<?php echo $features1_image; ?>" class="regular-text"/></td> 
        </tr>

        <tr><td colspan="2"><hr></td></tr>

        <tr valign="top">  
          <th scope="row"><label for="features2_title">Feature 2 Title</label></th>  
          <td><input type="text" name="features2_title" value="<?php echo $features2_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features2_body">Feature 2 Body</label></th>  
          <td><input type="text" name="features2_body" value="<?php echo $features2_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features2_image">Feature 2 Image</label></th>  
          <td><input type="text" name="features2_image" value="<?php echo $features2_image; ?>" class="regular-text"/></td> 
        </tr>

        <tr><td colspan="2"><hr></td></tr>

        <tr valign="top">  
          <th scope="row"><label for="features3_title">Feature 3 Title</label></th>  
          <td><input type="text" name="features3_title" value="<?php echo $features3_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features3_body">Feature 3 Body</label></th>  
          <td><input type="text" name="features3_body" value="<?php echo $features3_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features3_image">Feature 3 Image</label></th>  
          <td><input type="text" name="features3_image" value="<?php echo $features3_image; ?>" class="regular-text"/></td> 
        </tr>

        <tr><td colspan="2"><hr></td></tr>

        <tr valign="top">  
          <th scope="row"><label for="features4_title">Feature 4 Title</label></th>  
          <td><input type="text" name="features4_title" value="<?php echo $features4_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features4_body">Feature 4 Body</label></th>  
          <td><input type="text" name="features4_body" value="<?php echo $features4_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features4_image">Feature 4 Image</label></th>  
          <td><input type="text" name="features4_image" value="<?php echo $features4_image; ?>" class="regular-text"/></td> 
        </tr>

        <tr><td colspan="2"><hr></td></tr>

        <tr valign="top">  
          <th scope="row"><label for="features5_title">Feature 5 Title</label></th>  
          <td><input type="text" name="features5_title" value="<?php echo $features5_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features5_body">Feature 5 Body</label></th>  
          <td><input type="text" name="features5_body" value="<?php echo $features5_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features5_image">Feature 5 Image</label></th>  
          <td><input type="text" name="features5_image" value="<?php echo $features5_image; ?>" class="regular-text"/></td> 
        </tr>

        <tr><td colspan="2"><hr></td></tr>

        <tr valign="top">  
          <th scope="row"><label for="features6_title">Feature 6 Title</label></th>  
          <td><input type="text" name="features6_title" value="<?php echo $features6_title; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features6_body">Feature 6 Body</label></th>  
          <td><input type="text" name="features6_body" value="<?php echo $features6_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="features6_image">Feature 6 Image</label></th>  
          <td><input type="text" name="features6_image" value="<?php echo $features6_image; ?>" class="regular-text"/></td> 
        </tr>



        
      </table>  
      <p><input type="submit" value="Save settings" class="button-primary"/></p>
    </form>  
  </div>  
<?php  }
add_action("admin_menu", "setup_theme_admin_menus"); ?>