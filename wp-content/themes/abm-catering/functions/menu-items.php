<?php 

/*--- CHANGE POSTS TO NEWS IN ADMIN ---*/ 

function change_post_menu_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'News';
  $submenu['edit.php'][5][0] = 'News';
  $submenu['edit.php'][10][0] = 'Add News';
  $submenu['edit.php'][16][0] = 'News Tags';
  echo '';
}
function change_post_object_label() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'News';
  $labels->singular_name = 'News';
  $labels->add_new = 'Add News';
  $labels->add_new_item = 'Add News';
  $labels->edit_item = 'Edit News';
  $labels->new_item = 'News';
  $labels->view_item = 'View News';
  $labels->search_items = 'Search News';
  $labels->not_found = 'No News found';
  $labels->not_found_in_trash = 'No News found in Trash';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );




/*--- ALLOW EDITORS TO VIEW APPEARANCE SECTION ---*/ 

// get the the role object
$role_object = get_role( 'editor' );

// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );




/*--- REMOVE UNREQUIRED ADMIN NAVIGATION OPTIONS ---*/ 

function remove_menus () {
global $menu;
  $restricted = array( __('Links'), __('Tools'), __('Comments'), __('Settings'));
  // $restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
  }
}
if ( !current_user_can('manage_options') ) {
  add_action('admin_menu', 'remove_menus');
}



/*--- HIDE VARIOUS CONTENT IF NOT ADMIN ---*/ 

// function hideSEO() {
//    echo '<style type="text/css">
//            #ghpseo-meta.postbox {
//               display: none;}
//          </style>';
// }
 
// if ( !current_user_can('manage_options') ) {
//   add_action('admin_head', 'hideSEO');
// }







?>