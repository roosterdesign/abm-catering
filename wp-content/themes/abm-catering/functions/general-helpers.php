<?php 


// Set path to assets
global $trimmedAssetPath;
$trimmedAssetPath = str_replace(get_bloginfo('url'), '', get_bloginfo('template_directory'));


// 
add_theme_support( 'automatic-feed-links' );


//AUTO GENERATE "READ" ON EXERPTS

function excerpt_read_more_link($output) {
  global $post;
  return $output . '<a href="'. get_permalink($post->ID) . '" class="read-more"> Read</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');



function new_excerpt_more($more) {
	global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Change Excerpt length
function custom_excerpt_length( $length ) {
	return 8;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999 );



/**
 * Conditional Page/Post Navigation Links
 * http://www.ericmmartin.com/conditional-pagepost-navigation-links-in-wordpress-redux/
 * If more than one page exists, return TRUE.
 */
function show_posts_nav() {
  global $wp_query;
  return ($wp_query->max_num_pages > 1);
}



// Graviry Forms

// add_filter( 'gform_confirmation_anchor', function() {
//     return 70;
// } );

add_filter( 'gform_confirmation_anchor', '__return_false' );


?>