<?php 

/*--- IMPORT CUSTOM META ---*/ 
 
// include 'meta-url.php';
 



 //hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_topics_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Brands', 'taxonomy general name' ),
    'singular_name' => _x( 'Brand', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Brands' ),
    'all_items' => __( 'All Brands' ),
    'parent_item' => __( 'Parent Topic' ),
    'parent_item_colon' => __( 'Parent Brand:' ),
    'edit_item' => __( 'Edit Brand' ), 
    'update_item' => __( 'Update Brand' ),
    'add_new_item' => __( 'Add New Brand' ),
    'new_item_name' => __( 'New Brand Name' ),
    'menu_name' => __( 'Brands' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('brands',array('post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'brand' ),
  ));
 
}



 
/*--- DEFINE CUSTOM POST TYPES ---*/ 
 
 add_action( 'init', 'codex_custom_init' );

function codex_custom_init() {

  $testimonialsLabels = array(
    'name' => _x('Testimonials', 'post type general name'),
    'singular_name' => _x('Testimonials', 'post type singular name'),
    'add_new' => _x('Add New', 'Testimonials'),
    'add_new_item' => __('Add New Testimonial'),
    'edit_item' => __('Edit Testimonial'),
    'new_item' => __('New Testimonial'),
    'all_items' => __('All Testimonials'),
    'view_item' => __('View Testimonial'),
    'search_items' => __('Search Testimonials'),
    'not_found' =>  __('No Testimonials found'),
    'not_found_in_trash' => __('No Testimonials found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Testimonials'
  );
  $testimonialsArgs = array(
    'labels' => $testimonialsLabels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'our-experience'),
    // 'rewrite' => true,
    'exclude_from_search' => false,
    'capability_type' => 'page',
    'has_archive' => false, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-format-quote',
    'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'revisions', 'post-formats' )
  );
  register_post_type('testimonials',$testimonialsArgs);



  $vacanciesLabels = array(
    'name' => _x('Vacancies', 'post type general name'),
    'singular_name' => _x('Vacancies', 'post type singular name'),
    'add_new' => _x('Add New', 'Vacancies'),
    'add_new_item' => __('Add New Vacancy'),
    'edit_item' => __('Edit Vacancy'),
    'new_item' => __('New Vacancy'),
    'all_items' => __('All Vacancies'),
    'view_item' => __('View Vacancy'),
    'search_items' => __('Search Vacancies'),
    'not_found' =>  __('No Vacancies found'),
    'not_found_in_trash' => __('No Vacancies found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Vacancies'
  );
  $vacanciesArgs = array(
    'labels' => $vacanciesLabels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array ( 'slug' => 'about/vacancies', 'with_front' => FALSE ),
    // 'rewrite' => true,
    'exclude_from_search' => false,
    'capability_type' => 'page',
    'has_archive' => false, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-id-alt',
    // 'taxonomies'  => array( 'brands' ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'revisions', 'post-formats' )
  );
  register_post_type('vacancies',$vacanciesArgs);


}

?>