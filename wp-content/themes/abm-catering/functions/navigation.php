<?php

/* REMOVE THE CLASS-ITUS THAT THE STANDARD NAV HAS */

// add_filter('nav_menu_css_class','remove_nav_menu_classes');
// function remove_nav_menu_classes($classes) {
//     //print_r($classes);
//     $current = array_search('current-menu-item', $classes); 
//     return array($classes[1], $classes[$current]); 
// }



/*--- ADD FIRST AND LAST CLASSES TO NAVIGATION LIST ITEMS ---*/ 

 function add_first_and_last($output) {
   $output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
   $output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
   $output = str_replace(' class="menu-item "', '', $output);
   $output = str_replace('menu-item "', 'menu-item"', $output);
   $output = str_replace('first-menu-item menu-item', 'first-menu-item', $output);
   $output = str_replace('last-menu-item menu-item', 'last-menu-item', $output);

   // now sort out the links - don't need no absolute bullshit here :) 
   $output = str_replace(get_bloginfo('url'), '', $output);
   
   return $output;
 }
 add_filter('wp_nav_menu', 'add_first_and_last');


 /*--- REMOVE IDS FROM THE NAVIGATION ARRAYS ---*/ 
  add_filter('nav_menu_item_id', 'remove_nav_ids');
  function remove_nav_ids($nav) {
    return array();
  }


?>