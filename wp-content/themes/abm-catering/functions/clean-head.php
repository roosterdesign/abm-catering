<?php 

/*--- REMOVE HEAD TAGS THAT ARE NOT REQUIRED  ---*/  

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Removes links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Removes to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link'); // Removes link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link'); // Removes the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link'); // Removes the index link
remove_action( 'wp_head', 'parent_post_rel_link'); // Removes the prev link
remove_action( 'wp_head', 'start_post_rel_link'); // Removes the start link
remove_action( 'wp_head', 'adjacent_posts_rel_link'); 
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head'); // relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator'); // Removes the WordPress version i.e. - WordPress 2.8.4
remove_action('wp_head', 'rel_canonical');
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); 

?>