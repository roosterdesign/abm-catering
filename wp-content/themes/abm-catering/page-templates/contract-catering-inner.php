<?php
/*
Template Name: Page - Contract Catering - Inner
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>


	<div class="sub-page-nav">
		<?php
			$parentTitle = get_the_title($post->post_parent); $parentLink = get_permalink($post->post_parent); wp_reset_query();
			$currentPageId = get_the_id();
			$count = 0;
		?>
		<div class="mobile-nav">
			<span><?php echo $parentTitle; ?><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
	    	<ul>
	    		<li><a href="<?php echo $parentLink; ?>"><?php echo $parentTitle; ?></a></li>
	        	<?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->post_parent, 'order' => 'ASC', 'orderby' => 'menu_order' );
					$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
				     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				        <?php $id = get_the_ID(); $currentClass= ($id == $currentPageId) ? " class='current'": ""; ?>
				        <li<?php echo $currentClass; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
				    <?php endwhile; ?>
				<?php endif; wp_reset_query(); ?>
			</ul>
		</div>
		<div class="container desktop-nav">
		  <ul>
	        <?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->post_parent, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>			     	
			     	<?php $id = get_the_ID(); $currentClass= ($id == $currentPageId) ? " class='current'": ""; ?>
			     	<?php $count++; ?>
			        <li<?php echo $currentClass; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><small>0<?php echo $count; ?></small><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
			</ul>			
		</div>

	</div>

	<div class="container-fluid">
		<div class="main">
			<div class="container">
				<div class="panel cf">
					<div class="content">
						<?php the_content(); ?>
						<a href="<?php the_field('button_href'); ?>" class="btn btn-primary"><?php the_field('button_text'); ?></a>
					</div>
					<?php $introBg = get_field('intro_background'); if( !empty($introBg) ): ?>
					<div class="img" style="background-image: url('<?php echo $introBg['url']; ?>')">						
						<div class="logo">
							<a href="<?php the_field('button_href'); ?>"><?php $logo = get_field('logo'); if( !empty($logo) ): ?><img src="<?php echo $logo['url']; ?>"><?php endif; ?></a>
						</div>
					</div>					
					<?php endif; ?>
				</div>			
			</div>
		</div>
	</div>

	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>