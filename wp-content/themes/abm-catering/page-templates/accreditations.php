<?php
/*
Template Name: Page - About - Accreditations
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php the_content() ; ?>
						<?php $logosMobile = get_field('logos_mobile'); if( !empty($logosMobile) ): ?><img src="<?php echo $logosMobile['url']; ?>" class="img-responsive visible-mobile"><?php endif; ?>
						<?php $logosDesktop = get_field('logos_desktop'); if( !empty($logosDesktop) ): ?><img src="<?php echo $logosDesktop['url']; ?>" class="img-responsive visible-desktop"><?php endif; ?>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>