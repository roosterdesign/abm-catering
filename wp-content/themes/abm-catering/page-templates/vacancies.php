<?php
/*
Template Name: Page - About - Vacancies
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>
	<div class="main">
		<div class="container">			
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php the_content(); ?>
						<?php $loop = new WP_Query( array( 'post_type' => 'vacancies', 'posts_per_page' => -1, 'orderby' => 'menu_order' ) ); ?>

						<form class="vacancy-filter cf">

							<h3>Filter:</h3>

							<div class="radio">
								<input type="radio" name="filterOption" value="all" id="all" checked="checked">
								<label for="all">All</label>
							</div>

							<div class="radio">
								<input type="radio" name="filterOption" value="operationalRoles" id="operationalRoles">
								<label for="operationalRoles">Operational Roles</label>
							</div>

							<div class="radio">
								<input type="radio" name="filterOption" value="headOfficeRoles" id="headOfficeRoles">
								<label for="headOfficeRoles">Head Office &amp; Management Roles</label>								
							</div>

						</form>


						<table class="table filterable">
							<thead>
								<tr>
									<th>Position</th>
									<th>Location</th>
									<th>Salary</th>
									<th>Posted on</th>
									<th>Closing Date</th>
									<th>Reference</th>
								</tr>
							</thead>
							<tbody>
							<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
								<tr data-role="<?php the_field('role'); ?>">
									<td class="position"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
									<td>
										<span class="mobile-label">Location</span>
										<?php the_field('location'); ?></td>
									<td>
										<span class="mobile-label">Salary</span>
										<?php the_field('salary'); ?></td>
									<td>
										<span class="mobile-label">Posted on</span>
										<?php echo get_the_date('d/m/y'); ?></td>
									<td>
										<span class="mobile-label">Closing Date</span>
										<?php the_field('closing_date'); ?></td>
									<td>
										<span class="mobile-label">Reference</span>
										<?php the_field('reference'); ?></td>
								</tr>
							<?php endwhile; wp_reset_query(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>