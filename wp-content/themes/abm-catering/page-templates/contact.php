<?php
/*
Template Name: Page - Contact
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block cf">
				<div class="content">
					<div class="inner">
						<?php the_content(); ?>
						<?php /*
						<h2>Get In Touch</h2>
						<hr>
						<h3>Address:</h3>
						<p>
							abm Catering Solutions<br>
							Eagle Court<br>
							Saltisford<br>
							Warwick<br>
							CV34 4AF
						</p>
						<p class="contact-details">
							<strong>t.</strong> 01926 498448<br>
							<strong>e.</strong> <a href="mailto:sales@abmcatering.co.uk" target="_blank">sales@abmcatering.co.uk</a><br>
							<strong>w.</strong> <a href="abmcatering.co.uk" target="_blank">abmcatering.co.uk</a><br>
						</p>
						*/ ?>
					</div>
				</div>
				<div class="form">
					<?php gravity_form( 3, false, false, true, null, true ); ?>	
				</div>								
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>