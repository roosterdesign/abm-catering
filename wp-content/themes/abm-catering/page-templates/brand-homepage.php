<?php
/*
Template Name: Page - Brand Homepage
*/
?>
<?php get_header(); ?>
<div id="pageWrap">

	<div class="container-fluid" id="brand-home-hero">
		<?php if( have_rows('hero_slider') ): ?>		
			<?php
				$numOfSlides = count( get_field('hero_slider') );				
				if($numOfSlides > 1): ?>
					<div class="owl-carousel">
				<?php endif; ?>
					<?php while ( have_rows('hero_slider') ) : the_row(); ?>		    
					    <?php $image = get_sub_field('slide'); if( !empty($image) ): ?><div class="slide" style="background-image: url('<?php echo $image['url']; ?>');"></div><?php endif; ?>				
				    <?php endwhile; ?>
			    <?php if($numOfSlides > 1): ?>
					</div>
				<?php endif; ?>
		<?php endif; ?>		
		<?php $heroLogo = get_field('hero_logo'); if( !empty($heroLogo) ): ?><img src="<?php echo $heroLogo['url']; ?>" class="logo"><?php endif; ?>
		<div class="intro">
			<div class="container">				
				<div class="right">
					<h2><?php the_field('hero_right_title'); ?></h2>
					<p><?php the_field('hero_right_body'); ?></p>
					<?php if($numOfSlides > 1): ?>
						<div class="custom-owl-nav">
							<span class="next"><i class='icon arrow-right-white'></i></span>
							<span class="prev"><i class='icon arrow-left-white'></i></span>
						</div>
					<?php endif; ?>					
				</div>
				<div class="cf"></div>
				<hr>
				<div class="left">
					<h1><?php the_field('hero_left_title'); ?></h1>
					<p><?php the_field('hero_left_body'); ?></p>
				</div>
			</div>
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/parent-subnav.php"); ?>

	<?php $welcomeBg = get_field('welcome_background'); ?>
	<div id="welcome-slider"<?php if( !empty($welcomeBg) ): ?> style="background-image: url('<?php echo $welcomeBg['url']; ?>')"<?php endif; ?>>
		<?php $numOfSlides = count( get_field('welcome_slider') ); ?>
		<div class="container <?php if($numOfSlides > 1): ?> owl-carousel<?php endif; ?>">
			<?php if( have_rows('welcome_slider') ): while ( have_rows('welcome_slider') ) : the_row(); ?>
				<div class="slide">
					<div class="content">
						<h2><?php the_sub_field('slide_title'); ?></h2>
						<hr>
						<p><?php the_sub_field('slide_body'); ?></p>
						<p class="more"><a href="<?php the_sub_field('slide_link_href'); ?>"><?php the_sub_field('slide_link_text'); ?><span class="icon arrow-right-white"></span></a></p>
					</div>
					<?php $welcomeSlideImage = get_sub_field('slide_image'); if( !empty($welcomeSlideImage) ): ?>
					<div class="img" style="background-image: url('<?php echo $welcomeSlideImage['url']; ?>')"></div>
					<?php endif; ?>
				</div>
		    <?php endwhile; endif; ?>			
		</div>
	</div>

	<?php $testimonialBg = get_field('testimonial_background'); ?>
	<div id="customer-testimonial-wrap"<?php if( !empty($testimonialBg) ): ?> style="background-image: url('<?php echo $testimonialBg['url']; ?>')"<?php endif; ?>>
		<div class="container">
			
			<div class="other-pages">
				<div class="row">
					<h2><?php the_field('other_pages_title'); ?></h2>
					<?php if( have_rows('other_pages') ): while ( have_rows('other_pages') ) : the_row(); ?>
						<div class="col-sm-4">
							<div class="inner"<?php $otherPagesImg = get_sub_field('image'); if( !empty($otherPagesImg) ): ?> style="background-image: url('<?php echo $otherPagesImg['url']; ?>')"<?php endif; ?>>
								<div class="details">
									<h3><?php the_sub_field('title'); ?></h3>
									<p><?php the_sub_field('body'); ?></p>
									<a href="<?php the_sub_field('link_href'); ?>" class="more">Read</a>
								</div>
							</div>					
						</div>
				    <?php endwhile; endif; ?>
				</div>				
			</div>

			<?php include(get_template_directory()."/page-templates/inc/testimonials.php"); ?>
				
		</div>
	</div>

	<?php /* 
		<div class="panel-wrap">
			<div class="container">
				<?php $rightImg = get_field('bottom_panel_right_img'); if( !empty($rightImg) ): ?><img src="<?php echo $rightImg['url']; ?>" class="img1"><?php endif; ?>
				<?php $leftImg = get_field('bottom_panel_left_img'); if( !empty($leftImg) ): ?><img src="<?php echo $leftImg['url']; ?>" class="img2"><?php endif; ?>
				<div class="panel cf">
					<div class="content">
						<h2><?php the_field('bottom_panel_title'); ?></h2>
						<hr>
						<p><?php the_field('bottom_panel_body'); ?></p>
						<a href="<?php the_field('bottom_panel_link_href'); ?>" class="btn btn-outline"><?php the_field('bottom_panel_link_text'); ?></a>
					</div>
					<?php $bottomPanelImage = get_field('bottom_panel_image'); if( !empty($bottomPanelImage) ): ?>
					<div class="img" style="background-image: url('<?php echo $bottomPanelImage['url']; ?>')"></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	*/ ?>
	

	<?php $bottomPanelBg = get_field('bottom_panel_background'); ?>
	<div class="panel-wrap-alt"<?php if( !empty($bottomPanelBg) ): ?> style="background-image: url('<?php echo $bottomPanelBg['url']; ?>')"<?php endif; ?>>
		<div class="container">
			<div class="panel">
				<div class="content">
					<h2><?php the_field('bottom_panel_title'); ?></h2>
					<hr>
					<p><?php the_field('bottom_panel_body'); ?></p>
					<p class="more"><a href="<?php the_field('bottom_panel_button_href'); ?>"><?php the_field('bottom_panel_link_text'); ?><span class="icon arrow-right-white"></span></a></p>
				</div>
				<?php $bottomPanelImage = get_field('bottom_panel_image'); if( !empty($bottomPanelImage) ): ?>
				<div class="img" style="background-image: url('<?php echo $bottomPanelImage['url']; ?>')"></div>
				<?php endif; ?>
			</div>

		</div>
	</div>

	<div id="features">	
		<?php $featuresImg = get_field('features__image'); if( !empty($featuresImg) ): ?>
		<div class="img" style="background-image: url('<?php echo $featuresImg['url']; ?>')"></div>
		<?php endif; ?>
		<?php include(get_template_directory()."/page-templates/inc/features.php"); ?>
	</div>

</div>

<?php get_footer(); ?>