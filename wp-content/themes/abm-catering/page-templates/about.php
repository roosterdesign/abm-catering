<?php
/*
Template Name: Page - About
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/parent-subnav.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block cf">
				<div class="content">
					<div class="inner">
						<?php the_content(); ?>
					</div>
				</div>
				<?php $image = get_field('intro_image'); if( !empty($image) ): ?>
				<div class="img" style="background-image: url('<?php echo $image['url']; ?>')"></div>
				<?php endif; ?>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>