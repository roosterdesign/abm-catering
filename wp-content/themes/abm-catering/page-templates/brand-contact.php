<?php
/*
Template Name: Page - Brand Contact
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php if (has_post_thumbnail( $post->ID ) ): $heroBg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog_hero' ); $heroBg = $heroBg[0]; else: $heroBg = "/wp-content/themes/abm-catering/img/hero-fallback.jpg"; endif; ?>
	<div class="container-fluid" id="brand-hero" style="background-image: url('<?php echo $heroBg; ?>');">		
		<div class="strip">
			<div class="container">
				<?php $heroLogo = get_field('hero_logo'); if( !empty($heroLogo) ): ?><img src="<?php echo $heroLogo['url']; ?>" class="logo"><?php endif; ?>
				<div class="inner">					
					<h1><?php the_field('hero_title'); ?></h1>
					<?php if(get_field('hero_body')): ?>
						<p><?php the_field('hero_body'); ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>

	<?php $formBg = get_field('form_background'); ?>
	<div class="main" <?php if( !empty($formBg) ): ?> style="background-image: url('<?php echo $formBg['url']; ?>')"<?php endif; ?>>
		<div class="container">
			<div class="block cf">
				<div class="content">
					<div class="inner">
						<?php the_content() ;?>
					</div>
				</div>
				<div class="form">
					<?php $formID = get_field('gravityform_id'); gravity_form( $formID, false, false, true, null, true ); ?>	
				</div>								
			</div>			
		</div>
	</div>


	<?php $testimonialBg = get_field('testimonial_background'); ?>
	<div id="customer-testimonial-wrap"<?php if( !empty($testimonialBg) ): ?> style="background-image: url('<?php echo $testimonialBg['url']; ?>')"<?php endif; ?>>
		<div class="container">
			<?php include(get_template_directory()."/page-templates/inc/testimonials.php"); ?>				
		</div>
	</div>

	<?php $bottomPanelBg = get_field('bottom_panel_background'); ?>
	<div class="panel-wrap-alt"<?php if( !empty($bottomPanelBg) ): ?> style="background-image: url('<?php echo $bottomPanelBg['url']; ?>')"<?php endif; ?>>
		<div class="container">
			<div class="panel">
				<div class="content">
					<h2><?php the_field('bottom_panel_title'); ?></h2>
					<hr>
					<p><?php the_field('bottom_panel_body'); ?></p>
					<p class="more"><a href="<?php the_field('bottom_panel_button_href'); ?>"><?php the_field('bottom_panel_link_text'); ?><span class="icon arrow-right-white"></span></a></p>
				</div>
				<?php $bottomPanelImage = get_field('bottom_panel_image'); if( !empty($bottomPanelImage) ): ?>
				<div class="img" style="background-image: url('<?php echo $bottomPanelImage['url']; ?>')"></div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div id="features">	
		<?php $featuresImg = get_field('features__image'); if( !empty($featuresImg) ): ?>
		<div class="img" style="background-image: url('<?php echo $featuresImg['url']; ?>')"></div>
		<?php endif; ?>
		<?php include(get_template_directory()."/page-templates/inc/features.php"); ?>
	</div>

</div>
<?php get_footer(); ?>