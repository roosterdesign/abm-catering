<?php
/*
Template Name: Page - Homepage
*/
?>
<?php get_header(); ?>
<div id="pageWrap">

	<div class="container-fluid" id="homepage-hero">
		<div class="next-section"><a href="#brands" class="icon arrow-down-dark">next</a></div>
		<h1><?php the_field('hero_title'); ?></h1>
		<div class="strip">
			<div class="container">
				<div class="inner">
					<?php the_field('intro_body'); ?>				
					<a href="<?php the_field('intro_button_href'); ?>" class="btn btn-primary"><?php the_field('intro_button_text'); ?></a>
				</div>
			</div>
		</div>
		<div class="mask"></div>
	</div>

	<?php /*
	<div class="container-fluid" id="home-hero">
		<div class="img"></div>
		<div class="next-section"><a href="#brands" class="icon arrow-down-dark">next</a></div>
		<div class="container">
			<h1><?php the_field('hero_title'); ?></h1>			
			<div class="inner">
				<?php the_field('intro_body'); ?>				
				<a href="<?php the_field('intro_button_href'); ?>" class="btn btn-primary"><?php the_field('intro_button_text'); ?></a>
			</div>
		</div>
	</div>
	*/ ?>

	<?php include(get_template_directory()."/page-templates/inc/brands.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>