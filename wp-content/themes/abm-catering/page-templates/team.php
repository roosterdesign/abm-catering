<?php
/*
Template Name: Page - About - Team
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="team-member-wrap">
				<?php if( have_rows('team_members') ): while ( have_rows('team_members') ) : the_row(); ?>
				        <div class="team-member cf">					
							<div class="bio">
								<h2><?php the_sub_field('name'); ?></h2>
								<p class="role"><?php the_sub_field('job_title'); ?></p>
								<hr>
								<?php the_sub_field('bio'); ?>
							</div>						
							<?php $image = get_sub_field('photo'); if( !empty($image) ): ?>
							<div class="img" style="background-image: url('<?php echo $image['url']; ?>')"></div>
							<?php endif; ?>
						</div>
			    <?php endwhile; endif; ?>
				
				<div class="key-personnel">

				    <h2 class="key-personnel">Other Key Personnel</h2>
				    <hr>

				    <?php if( have_rows('kp_team_members') ): while ( have_rows('kp_team_members') ) : the_row(); ?>
					        <div class="team-member cf">					
								<div class="bio">
									<h2><?php the_sub_field('kp_name'); ?></h2>
									<p class="role"><?php the_sub_field('kp_job_title'); ?></p>
									<hr>
									<?php the_sub_field('kp_bio'); ?>
								</div>						
								<?php $kp_image = get_sub_field('kp_photo'); if( !empty($kp_image) ): ?>
								<div class="img" style="background-image: url('<?php echo $kp_image['url']; ?>')"></div>
								<?php endif; ?>
							</div>
				    <?php endwhile; endif; ?>

			    </div>

			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>