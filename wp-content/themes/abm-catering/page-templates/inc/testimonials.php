<div class="testimonial cf">				
	<div class="col-right">
		<h3 class="bar"><?php the_field('testimonial_title'); ?></h3>
		<?php $posts = get_field('testimonials_picker'); if( $posts ): $numOfSlides = count( get_field('testimonials_picker') ); ?>	
			<?php if($numOfSlides > 1): ?><div class="custom-owl-nav"><span class="prev"><i class='icon arrow-left-white'></i></span><span class="next"><i class='icon arrow-right-white'></i></span></div><?php endif; ?>
			<div class="cf"></div>						
			<?php if($numOfSlides > 1): ?><div class="owl-carousel"><?php endif; ?>
				    <?php foreach( $posts as $post): ?>
				        <?php setup_postdata($post); ?>									
				        <div class="slide quote">
							<?php the_content(); ?>
							<span class="cite"><?php the_field('cite'); ?>	</span>
						</div>	
				    <?php endforeach; ?>
			    <?php if($numOfSlides > 1): ?></div><?php endif; ?>
		<?php wp_reset_postdata(); endif; ?>
	</div>
</div>