<div class="sub-page-nav">
	<?php
		$parentTitle = get_the_title($post->post_parent); $parentLink = get_permalink($post->post_parent); wp_reset_query();
		$currentPageId = get_the_id();
		$count = 1;
	?>
	<div class="mobile-nav">
		<span><?php the_title(); ?><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
		<ul>
			<?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
	</div>
	<div class="container desktop-nav">
		<ul>
		  	<li class="current"><a href="<?php the_permalink(); ?>"><small>0<?php echo $count; ?></small><?php the_title(); ?></a></li>
	        <?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>			     	
			     	<?php $count++; ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><small>0<?php echo $count; ?></small><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
	</div>
</div>