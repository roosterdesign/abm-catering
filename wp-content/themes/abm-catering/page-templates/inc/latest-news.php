<div class="container-fluid" id="latest-news">
	<div class="container news-wrap">
		<h2>Latest News</h2>
		<?php global $post; $args = array( 'posts_per_page' => 3 ); $myposts = get_posts( $args ); ?>
		<div class="row">
			<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
			<article class="col-sm-6 col-lg-4">
				<?php
					if(get_post_thumbnail_id()):
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_image_src($thumb_id, 'blog_thumbnail', true);
						$thumb_url = $thumb_url[0];
					else:
						$thumb_url = '/wp-content/themes/abm-catering/img/no-blog-thumb.gif';
					endif;					
				?>
				<div class="inner" style="background-image: url('<?php echo $thumb_url; ?>') ;"> 
					<div class="details">
						<div class="date">
							<p><?php echo get_the_date('d '); ?><span><?php echo get_the_date('M'); ?></span></p>
						</div>
						<div class="desc">
							<p class="title"><a href="<?php the_permalink(); ?>"><?php if (strlen($post->post_title) > 40) : echo substr(the_title($before = '', $after = '', FALSE), 0, 40) . '...'; else: the_title(); endif; ?></a></p>
							<?php the_excerpt(); ?>
						</div>
					</div>
				</div>					
			</article>
			<?php endforeach; ?>
		</div>
		<?php wp_reset_postdata();?>
		<div class="more-news">
			<div class="btn-wrap">
				<a href="/news" class="btn btn-outline">See more news</a>
			</div>
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/accreditations.php"); ?>
</div>