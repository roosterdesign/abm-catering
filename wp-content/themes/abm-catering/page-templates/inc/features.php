<?php
	$features1_title = get_option("features1_title");
	$features1_body = get_option("features1_body");
	$features1_image = get_option("features1_image");
	$features2_title = get_option("features2_title");
	$features2_body = get_option("features2_body");
	$features2_image = get_option("features2_image");
	$features3_title = get_option("features3_title");
	$features3_body = get_option("features3_body");
	$features3_image = get_option("features3_image");
	$features4_title = get_option("features4_title");
	$features4_body = get_option("features4_body");
	$features4_image = get_option("features4_image");
	$features5_title = get_option("features5_title");
	$features5_body = get_option("features5_body");
	$features5_image = get_option("features5_image");
	$features6_title = get_option("features6_title");
	$features6_body = get_option("features6_body");
	$features6_image = get_option("features6_image");
?>
<div class="container">			
	<ul>
		<li>
			<img src="<?php echo $features1_image; ?>" />
			<h3><?php echo $features1_title; ?></h3>
			<p><?php echo $features1_body; ?></p>
		</li>
		<li>
			<img src="<?php echo $features2_image; ?>" />
			<h3><?php echo $features2_title; ?></h3>
			<p><?php echo $features2_body; ?></p>
		</li>
		<li>
			<img src="<?php echo $features3_image; ?>" />
			<h3><?php echo $features3_title; ?></h3>
			<p><?php echo $features3_body; ?></p>
		</li>
		<li>
			<img src="<?php echo $features4_image; ?>" />
			<h3><?php echo $features4_title; ?></h3>
			<p><?php echo $features4_body; ?></p>
		</li>
		<li>
			<img src="<?php echo $features5_image; ?>" />
			<h3><?php echo $features5_title; ?></h3>
			<p><?php echo $features5_body; ?></p>
		</li>
		<li>
			<img src="<?php echo $features6_image; ?>" />
			<h3><?php echo $features6_title; ?></h3>
			<p><?php echo $features6_body; ?></p>
		</li>
	</ul>
</div>