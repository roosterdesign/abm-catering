<div id="making-a-difference" class="container-fluid">
	<div class="container">
		<div class="main-content">
			<h2 class="bar">Subheading title</h2>
			<h3>Making A Difference</h3>
			<hr>
			<div class="row">
				<div class="col-xlg-6">
					<p>We are proud to be an independent fresh-food catering contractor, founded in 1983, providing a high-quality service that brings the best in fantastic and innovative food. Our approach is simple yet highly effective; we source our produce through a community of responsible local suppliers, farmers and growers, who share the same genuine passion for great food as we do.</p>
				</div>
				<div class="col-xlg-6">
					<p>We believe that no two clients' needs are the same, which is why all of our services are tailored to your exact requirements.</p>
				</div>				
			</div>
			<ul class="features">
				<li>
					<img src="/wp-content/themes/abm-catering/img/home/home-people.png" />
					<p>Over 1500 team members</p>
				</li>
				<li>
					<img src="/wp-content/themes/abm-catering/img/home/home-meals.png" />
					<p>Over 35,000 meals served daily</p>
				</li>
				<li>
					<img src="/wp-content/themes/abm-catering/img/home/home-star.png" />
					<p>Transparent management</p>
				</li>
				<li>
					<img src="/wp-content/themes/abm-catering/img/home/home-pin.png" />
					<p>176 locations</p>
				</li>
			</ul>
		</div>
		<div class="img" style="background-image: url('/wp-content/themes/abm-catering/img/making-a-difference.jpg')"><a href="/about/" class="btn btn-secondary">About us</a></div>
	</div>
</div>