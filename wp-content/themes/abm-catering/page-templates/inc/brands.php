<div id="brands" class="container-fluid">
	<div class="container">
		<h2>Five brands, one family</h2>

		<a href="/brands/venue/" class="brand-banner" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/venue-bg.jpg')">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/venue-logo.png" class="logo">
			<h3>Stadia &amp; Venue</h3>
			<p>Find out more <span class="icon arrow-right-white"></span></p>
		</a>

		<a href="/brands/honest/" class="brand-banner" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/honest-bg.jpg')">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/honest-logo.png" class="logo">
			<h3>Business &amp; Industry</h3>
			<p>Find out more <span class="icon arrow-right-white"></span></p>
		</a>

		<a href="/brands/essence/" class="brand-banner" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/essence-bg.jpg')">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/essence-logo.png" class="logo">
			<h3>Assisted Living</h3>
			<p>Find out more <span class="icon arrow-right-white"></span></p>
		</a>

		<a href="/brands/mint/" class="brand-banner" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/mint-bg.jpg')">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/mint-logo.png" class="logo">
			<h3>Secondary &amp; Further Education</h3>
			<p>Find out more <span class="icon arrow-right-white"></span></p>
		</a>

		<a href="/brands/grow/" class="brand-banner" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/grow-bg.jpg')">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/grow-logo-mobile.png" class="logo visible-mobile">
			<img src="<?php echo $trimmedAssetPath; ?>/img/home/grow-logo.png" class="logo visible-desktop">
			<h3>Primary &amp; Nursery Education</h3>
			<p>Find out more <span class="icon arrow-right-white"></span></p>
		</a>

	</div>
</div>