<?php if (has_post_thumbnail( $post->ID ) ): $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog_hero' ); $image = $image[0]; else: $image = "/wp-content/themes/abm-catering/img/hero-fallback.jpg"; endif; ?>
<div class="container-fluid" id="hero" style="background-image: url('<?php echo $image; ?>');">
	<div class="strip">
		<div class="container">
			<div class="inner">
				<h1><?php if(is_404()): echo 'Error 404'; else: the_title(); endif; ?></h1>				
			</div>
		</div>
	</div>
</div>