<?php
/*
Template Name: Page - Contract Catering
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	
	<div class="sub-page-nav">
	<?php
		$parentTitle = get_the_title($post->post_parent); $parentLink = get_permalink($post->post_parent); wp_reset_query();
		$currentPageId = get_the_id();
		$count = 0;
	?>
	<div class="mobile-nav">
		<span><?php the_title(); ?><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
		<ul>
			<?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
	</div>
	<div class="container desktop-nav">
		<ul>
	        <?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>			     	
			     	<?php $count++; ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><small>0<?php echo $count; ?></small><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
	</div>
</div>

	<div class="main">
		<div class="container">
			<div class="panel cf">
				<div class="content">
					<?php the_content(); ?>
				</div>			
				<?php $introImage = get_field('intro_image'); if( !empty($introImage) ): ?>
				<div class="img" style="background-image: url('<?php echo $introImage['url']; ?>')"></div>
				<?php endif; ?>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/brands.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>