<?php
/*
Template Name: Page - Generic Thank You
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	
	<?php if (has_post_thumbnail( $post->ID ) ): $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog_hero' ); $image = $image[0]; else: $image = "/wp-content/themes/abm-catering/img/hero-fallback.jpg"; endif; ?>
	<div class="container-fluid" id="hero" style="background-image: url('<?php echo $image; ?>');">
		<div class="strip">
			<div class="container">
				<div class="inner">
					<h1>Thank You</h1>				
				</div>
			</div>
		</div>
	</div>

	<div class="main">
		<div class="container">
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php the_content() ; ?>
						<?php $logosMobile = get_field('logos_mobile'); if( !empty($logosMobile) ): ?><img src="<?php echo $logosMobile['url']; ?>" class="img-responsive visible-mobile"><?php endif; ?>
						<?php $logosDesktop = get_field('logos_desktop'); if( !empty($logosDesktop) ): ?><img src="<?php echo $logosDesktop['url']; ?>" class="img-responsive visible-desktop"><?php endif; ?>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>