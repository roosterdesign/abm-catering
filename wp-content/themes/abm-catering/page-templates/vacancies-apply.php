<?php
/*
Template Name: Page - About - Vacancies (Form)
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>
	<div class="main">
		<div class="container">			
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php /* $jobRef = $_GET['title']; $jobRef = str_replace('_', ' ', $jobRef);  */ ?>
						<?php the_content(); ?>
					</div>
				</div>
			</div>	

			<div class="block form-wrap">	
				<div class="form">
					<?php gravity_form( 1, false, false, true, null, true ); ?>					
				</div>
				<?php $image1 = get_field('image_1'); if( !empty($image1) ): ?>
				<div class="img imgtop" style="background-image: url('<?php echo $image1['url']; ?>')"></div>
				<?php endif; ?>
				<?php $image2 = get_field('image_2'); if( !empty($image2) ): ?>
				<div class="img imgbottom" style="background-image: url('<?php echo $image2['url']; ?>')"></div>
				<?php endif; ?>
			</div>

		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>