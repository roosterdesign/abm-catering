<?php
/*
Template Name: Page - Brands Landing Page
*/
?>
<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	
	<div class="sub-page-nav">
	<?php
		$parentTitle = get_the_title($post->post_parent); $parentLink = get_permalink($post->post_parent); wp_reset_query();
		$currentPageId = get_the_id();
		$count = 0;
	?>
	<div class="mobile-nav">
		<span><?php the_title(); ?><i class="fa fa-chevron-down" aria-hidden="true"></i></span>

		<?php /*
		<ul>
			<?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
		*/ ?>

		<ul>
			<li><a href="/brands/venue/" title="Venue - Stadia & Venue">Venue <br><small>Stadia &amp; Venue</small></a></li>
			<li><a href="/brands/honest/" title="Honest - Business & Industry">Honest <br><small>Business &amp; Industry</small></a></li>
			<li><a href="/brands/essence/" title="Essence - Assisted Living & Healthcare">Essence <br><small>Assisted Living &amp; Healthcare</small></a></li>
			<li><a href="/brands/mint/" title="Mint - Secondary & Further Education">Mint <br><small>Secondary &amp; Further Education</small></a></li>
			<li><a href="/brands/grow/" title="Grow - Primary & Nursery Education">Grow <br><small>Primary &amp; Nursery Education</small></a></li>
		</ul>

	</div>
	<div class="container desktop-nav">

		<?php /*
		<ul>
	        <?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'order' => 'ASC', 'orderby' => 'menu_order' );
				$parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
			     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>			     	
			     	<?php $count++; ?>
			        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><small>0<?php echo $count; ?></small><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
		*/ ?>


		<ul>
			<li><a href="/brands/venue/" title="Venue - Stadia & Venue"><small>01</small>Venue <br><span>Stadia &amp; Venue</span></a></li>
			<li><a href="/brands/honest/" title="Honest - Business & Industry"><small>02</small>Honest <br><span>Business &amp; Industry</span></a></li>
			<li><a href="/brands/essence/" title="Essence - Assisted Living & Healthcare"><small>03</small>Essence <br><span>Assisted Living &amp; Healthcare</span></a></li>
			<li><a href="/brands/mint/" title="Mint - Secondary & Further Education"><small>04</small>Mint <br><span>Secondary &amp; Further Education</span></a></li>
			<li><a href="/brands/grow/" title="Grow - Primary & Nursery Education"><small>05</small>Grow <br><span>Primary &amp; Nursery Education</span></a></li>
		</ul>



	</div>
</div>

	<div class="main">
		<div class="container">
			<div class="panel cf">
				<div class="content">
					<?php the_content(); ?>
				</div>			
				<?php $introImage = get_field('intro_image'); if( !empty($introImage) ): ?>
				<div class="img" style="background-image: url('<?php echo $introImage['url']; ?>')"></div>
				<?php endif; ?>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/brands.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>