<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<h2>Page not found</h2>
						<p>Please <a href="/">click here</a> to return to the homepage.</p>					
					</div>
				</div>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>