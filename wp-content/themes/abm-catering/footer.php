		<footer id="site-footer">
			<div class="container">

				<div class="row">
					<div class="col-sm-4 col-md-3">
						<h3><small>01</small>Links</h3>
						<?php wp_nav_menu( array( 'menu' => 'Main Nav', 'container' => '', 'depth' => 1) ); ?>
					</div>
					<div class="col-sm-4 col-md-3">
						<h3><small>02</small>Sectors</h3>
						<?php $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => '14', 'order' => 'ASC', 'orderby' => 'menu_order' );
							$parent = new WP_Query( $args );
							if ( $parent->have_posts() ) : ?>
							     <ul>
								     <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
								        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>       
								    <?php endwhile; ?>
							    </ul>
						<?php endif; wp_reset_query(); ?>
					</div>
					<div class="col-sm-4 col-md-3 contact">
						<h3><small>03</small>Contact us</h3>
						<ul>
							<li>
								T: 01926 498 448<br>
								E: <a href="mailto:info@abmcatering.com">info@abmcatering.com</a>
							</li>
							<li>
								Address:<br> abm catering,<br> Eagle Court,<br> 63-67 Saltisford,<br> Warwick,<br> CV34 4AF
							</li>
						</ul>						
					</div>
					<div class="col-sm-12 col-md-3">
						<h3><small>04</small>Sign up to our mailing list</h3>
						<?php gravity_form( 2, false, false, true, '', true, 32 ); ?>	
						<ul class="social">
							<li><a href="https://www.facebook.com/abmcatering/?ref=bookmarks" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/abmcateringltd" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/company-beta/11135841" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/abmcatering/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>

				<p class="copyright">&copy; abm catering solutions <?php echo date('Y'); ?><span>|</span><a href="http://www.auburn.co.uk" title="Auburn Creative" target="_blank">Website design by Auburn creative</a></p>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>
