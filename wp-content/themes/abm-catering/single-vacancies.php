<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>

	<div class="sub-page-nav">
		<div class="mobile-nav">
			<span>Vacancies<i class="fa fa-chevron-down" aria-hidden="true"></i></span>
			<ul>
				<li><a href="/about/team/" title="Team">Team</a></li>
				<li><a href="/about/vacancies/" title="Vacancies">Vacancies</a></li>
				<li><a href="/about/accreditations/" title="Accreditations">Accreditations</a></li>
			</ul>
		</div>
		<div class="container desktop-nav">
			<ul>
				<li><a href="/about/"><small>01</small>About</a></li>
				<li><a href="/about/team/" title="Team"><small>02</small>Team</a></li>
				<li class="current"><a href="/about/vacancies/" title="Vacancies"><small>03</small>Vacancies</a></li>
				<li><a href="/about/accreditations/" title="Accreditations"><small>04</small>Accreditations</a></li>
			</ul>
		</div>
	</div>

	<div class="main">
		<div class="container">			
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<ul class="overview">
							<li>
								<span class="label">Location</span>
								<?php the_field('location'); ?></li>
							<li>
								<span class="label">Salary</span>
								<?php the_field('salary'); ?></li>
							<li>
								<span class="label">Posted on</span>
								<?php echo get_the_date('d/m/y'); ?></li>
							<li>
								<span class="label">Closing date</span>
								<?php the_field('closing_date'); ?></li>
							<li>
								<span class="label">Reference</span>
								<?php the_field('reference'); ?></li>
						</ul>
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>						
						<?php $jobRef = get_the_title(); $jobRef = str_replace(' ', '_', $jobRef); ?>
						<a href="/about/job-application/?title=<?php echo $jobRef; ?>" class="btn btn-primary">Apply for this role</a>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>