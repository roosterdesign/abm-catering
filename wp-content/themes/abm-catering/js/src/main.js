$(document).ready(function(){

	// Mobile Nav
	function mobileNav() {

		$('nav .menu-item-has-children').append( "<span></span>");

		$('.nav-toggle').click(function(){
			if ($(this).hasClass('open')) {
				$('.nav-toggle').removeClass('open');
				$('#site-header nav').stop().animate({
					'right': '-280px'
				}, 700, 'easeInOutExpo', function(){
					$('body').removeClass('noscroll');
					$('#site-header nav .menu-item-has-children').removeClass('open');
					// $('.sub-menu').stop().slideUp(350);
					$('.sub-menu').hide();
				});
			} else {
				$('body').addClass('noscroll');
				$('.nav-toggle').addClass('open');
				$('#site-header nav').stop().animate({
					'right': '0'
				}, 700, 'easeInOutExpo');
				// $('body').on('click', 'nav .menu-item-has-children span', function(e){
				$('nav .menu-item-has-children span').click(function(e){
					//if(e.target.tagName == "LI") {
						$('nav .menu-item-has-children').removeClass('open');
						// $('.sub-menu').stop().slideUp(350);
						// $(this).addClass('open').children('.sub-menu').stop().slideDown(700, 'easeInOutExpo');
						$(this).parents('.menu-item-has-children').addClass('open').children('.sub-menu').show();
		
						$('nav .menu-item-has-children.open span').click(function(e){	
							//if(e.target.tagName == "LI") {		
							// $(this).removeClass('open').children('.sub-menu').stop().slideUp(700, 'easeInOutExpo');
							$(this).parents('.menu-item-has-children').removeClass('open').children('.sub-menu').hide();
							//}
						});
					//}
				});
			}
		});
	};
	mobileNav();


	// Home next section
	$('body.home .next-section a').click(function(e){
		e.preventDefault();
		var $target = $(this).attr('href');
		var $headerHeight = $('#site-header').height();
		$('html, body').animate({
	        scrollTop: $($target).offset().top - $headerHeight
	    }, 1200, 'easeInOutExpo');
	});


	// Sub Nav Dropdown (Mobile)
	$('.sub-page-nav .mobile-nav span').click(function() {
		console.log('clicked');
		$(this).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
		$('.sub-page-nav .mobile-nav ul').slideToggle(250);
	});


	// Custom File Upload
	$( 'input[type="file"]' ).each( function() {
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();
			if( fileName ) {
				console.log($(this));
				$(this).parents('.file-upload').find('.gfield_description').prepend('<span class="filename">'+fileName+'</span>');
				//$('.file-upload .gfield_description' ).prepend('<span class="filename">'+fileName+'</span>');
			} else {
				$label.html( labelVal );
			}
		});
		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});


	// Brands Homepage Hero Carousel
	(function brandHomeCarousel() {
		$('#brand-home-hero .owl-carousel').owlCarousel({
			items: 1,
			dots: false,
			margin: 0,
			loop: true
		});
		owl = $('#brand-home-hero .owl-carousel').owlCarousel();
		$("#brand-home-hero .prev").click(function () { owl.trigger('prev.owl.carousel'); });
		$("#brand-home-hero .next").click(function () { owl.trigger('next.owl.carousel'); });
	})();



	// Brands Testimonial Carousel
	(function testimonialCarousel() {
		$('.testimonial .owl-carousel').owlCarousel({
			items: 1,
			dots: false,
			margin: 20,
			loop: true,
			autoHeight: true,
			autoplay: true,
			autoplayHoverPause: true,
			autoplayTimeout: 10000
		});
		owl = $('.testimonial .owl-carousel').owlCarousel();
		$(".testimonial .prev").click(function () { owl.trigger('prev.owl.carousel'); });
		$(".testimonial .next").click(function () { owl.trigger('next.owl.carousel'); });
	})();


	// Brands Homepage Welcome Carousel
	$('#welcome-slider .owl-carousel').owlCarousel({
		items: 1,
		margin: 40,
		nav: true,
		loop: true,
		navText: ["<i class='icon arrow-left-dark' aria-hidden='true'></i>","<i class='icon arrow-right-dark' aria-hidden='true'></i>"],
		responsive : {
		    0 : {
		        nav: false,
				dots: true
		    },
		    769 : {
		        nav: true,
				dots: false
		    }
		}
	});


	// Match Heights
	$('#features ul li p').matchHeight();
	$('#customer-testimonial-wrap .other-pages .inner .details p').matchHeight();


	// Vacancies Form Job Title
	function jobPositionFormField(){

		$('form#gform_1 .position input').attr("readonly","readonly");

		function getUrlVars() {
		    var vars = [], hash;
		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++)
		    {
		        hash = hashes[i].split('=');
		        vars.push(hash[0]);
		        vars[hash[0]] = hash[1];
		    }
		    return vars;
		}
		var $jobTitle = getUrlVars()["title"];
		$jobTitle = $jobTitle.replace(/_/g, ' ');
		$('form#gform_1 .position input').val($jobTitle);
		};
	if ( $('form#gform_1').length > 0) { jobPositionFormField() };


	// Vacancy Filter
	$('form.vacancy-filter input').change(function() {
		var $selectedRole = $("form.vacancy-filter input:checked").val();
		if ( $selectedRole == "all") {
			$('.filterable tbody').find('tr').show();
		} else {
			$('.filterable tbody tr').hide();
			$('.filterable tbody').find("tr[data-role='" + $selectedRole + "']").show();
		}		
	});


	// Custom Radio
    //$('input:checkbox:checked').parent('.checkbox, .slider-checkboxes').find('label').addClass("checked");
    $('input:radio:checked').parent('.radio').addClass("checked");
    $("body").on('change', '.radio input:radio', function () {
    	var self = $(this);

    	$('.radio').removeClass("checked");
        
        if (self.is(":checked")) {
            //self.parent('.checkbox, .slider-checkboxes').find('label').addClass("checked");
            self.parent('.radio').addClass("checked");
        } else {
            //self.parent('.radio').removeClass("checked");
        }
    });




});