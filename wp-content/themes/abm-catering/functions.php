<?php
/**
 * @package WordPress
 */

define('MY_WORDPRESS_FOLDER',$_SERVER['DOCUMENT_ROOT']);
define('MY_THEME_FOLDER',str_replace('\\','/',dirname(__FILE__)));
define('MY_THEME_PATH','/' . substr(MY_THEME_FOLDER,stripos(MY_THEME_FOLDER,'wp-content'))); 
require( get_template_directory() . '/functions/clean-head.php' );
require( get_template_directory() . '/functions/login.php' );
require( get_template_directory() . '/functions/menu-items.php' );
require( get_template_directory() . '/functions/navigation.php' );
require( get_template_directory() . '/functions/site-settings.php' );
require( get_template_directory() . '/functions/image-settings.php' );
require( get_template_directory() . '/functions/custom-post-types.php' );
require( get_template_directory() . '/functions/comments.php' );
require( get_template_directory() . '/functions/general-helpers.php' );
require( get_template_directory() . '/functions/dashboard.php' );
require( get_template_directory() . '/functions/shortcodes.php' );
require( get_template_directory() . '/functions/sidebars.php' );
require( get_template_directory() . '/functions/enqueue.php' );


// ADD THESE TO WP-CONFIG

// DISALOW FILE EDITING FROM ADMIN: 
// define('DISALLOW_FILE_EDIT', true); 
// define( 'AUTOMATIC_UPDATER_DISABLED', true );


// ============ prevent subscriber level users from accessing wp admin
 
add_action( 'init', 'blockusers_init' );
function blockusers_init() {
    if ( is_admin() && is_user_logged_in()  && !current_user_can( 'publish_posts' ) && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}


// CUSTOM ADMIN MENU LINK FOR ALL SETTINGS
function all_settings_link() {
    add_options_page(__('All Settings'), __('All Settings'), 'administrator', 'options.php');
}
add_action('admin_menu', 'all_settings_link');


// add custom WYSIWYG editor styles 
add_editor_style('css/wysiwyg.css');


// helper function to retrieve attachment ID from the image URL
// http://pippinsplugins.com/retrieve-attachment-id-from-image-url/
function aub_get_image_id($image_url) {
   global $wpdb;
   $prefix = $wpdb->prefix;
   $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM " . $prefix . "posts" . " WHERE guid='%s';", $image_url )); 
   return $attachment[0]; 
}

// FIX PAGE CATEGORY TREE
function taxonomy_checklist_checked_ontop_filter ($args) {
  $args['checked_ontop'] = false;
  return $args;
}
add_filter('wp_terms_checklist_args','taxonomy_checklist_checked_ontop_filter');
add_theme_support( 'title-tag' );

function trim_content($x, $length) {
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}

  // Lower SEO meta box
  add_filter( 'wpseo_metabox_prio', function() { return 'low';});

  // Yoast CPT Breadcrumbs
  add_filter( 'wpseo_breadcrumb_links', 'my_wpseo_breadcrumb_links' );
  function my_wpseo_breadcrumb_links( $links ) {
   
      if ( is_single() ) {
          $cpt_object = get_post_type_object( get_post_type() );
          if ( ! $cpt_object->_builtin ) {
              $landing_page = get_page_by_path( $cpt_object->rewrite['slug'] );
              array_splice( $links, -1, 0, array(
                  array(
                      'id'    => $landing_page->ID
                  )
              ));
          }
      }
   
      return $links;
  }

add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );

  function add_current_nav_class($classes, $item) {
  
  // Getting the current post details
  global $post;
  
  // Getting the post type of the current post
  $current_post_type = get_post_type_object(get_post_type($post->ID));
  $current_post_type_slug = $current_post_type->rewrite['slug'];
    
  // Getting the URL of the menu item
  $menu_slug = strtolower(trim($item->url));
  
  // If the menu item URL contains the current post types slug add the current-menu-item class
  if (strpos($menu_slug,$current_post_type_slug) !== false) {
  
     $classes[] = 'current-menu-item';
  
  } else {
    
    $classes = array_diff( $classes, array( 'current_page_parent' ) );
  }
  
  // Return the corrected set of classes to be added to the menu item
  return $classes;

}


// Hide editor certain pages
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if(
    $template_file == 'page-templates/homepage.php' || $template_file == 'page-templates/what-we-do-list.php' || $template_file == 'page-templates/calendar-template.php'
    ){
      remove_post_type_support('page', 'editor');
  }
}


?>