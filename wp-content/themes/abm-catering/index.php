<?php get_header(); ?>

<div id="pageWrap">
	<div class="container-fluid" id="hero" style="background-image: url('<?php echo $trimmedAssetPath; ?>/img/home/hero-placeholder.jpg');">
		<div class="strip">
			<div class="container">
				<div class="inner">
					<h1><?php if ( is_home() ) : single_post_title(); elseif ( is_category() || is_archive() ) : echo single_cat_title(); endif; ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid" id="latest-news">
		<div class="container">

			<div class="row">
				<div class="col-md-7 col-lg-8">
					<div class="news-wrap">
						<?php if ( have_posts() ) : ?>
							<div class="row">
								<?php while ( have_posts() ) : the_post(); ?>
									<article class="col-sm-6 col-md-12 col-lg-6">
										<?php
											if(get_post_thumbnail_id()):
												$thumb_id = get_post_thumbnail_id();
												$thumb_url = wp_get_attachment_image_src($thumb_id, 'blog_thumbnail', true);
												$thumb_url = $thumb_url[0];
											else:
												$thumb_url = '/wp-content/themes/abm-catering/img/no-blog-thumb.gif';
											endif;					
										?>
										<div class="inner" style="background-image: url('<?php echo $thumb_url; ?>') ;"> 
											<div class="details">
												<div class="date">
													<p><?php echo get_the_date('d '); ?><span><?php echo get_the_date('M'); ?></span></p>
												</div>
												<div class="desc">
													<p class="title"><a href="<?php the_permalink(); ?>"><?php if (strlen($post->post_title) > 40) : echo substr(the_title($before = '', $after = '', FALSE), 0, 40) . '...'; else: the_title(); endif; ?></a></p>
													<?php the_excerpt(); ?>
												</div>
											</div>
										</div>
									</article>
								<?php endwhile; ?>
							</div>
							
							<?php if( get_previous_posts_link() || get_next_posts_link()) : ?>
							<div class="navigation cf">
								<div class="newer"><?php previous_posts_link( 'Newer posts' ); ?></div>
								<div class="older"><?php next_posts_link( 'Older posts' ); ?></div>
							</div>
							<?php endif; ?>

						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-5 col-lg-4">
					<div class="tweets-wrap">
						<h2>Tweets</h2>
						<?php echo do_shortcode( '[custom-twitter-feeds]' ); ?>
					</div>
				</div>
			</div>

			<?php /*
			<h2>What's New</h2>
			<p>Keep up to date with our latest news!</p>
			<ul class="categories-list">
				<?php wp_list_categories(array('title_li' => 'Categories:', 'orderby' => 'term_order' )); ?>
			</ul>
			<?php if ( have_posts() ) : ?>
				<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
						<article class="col-sm-6 col-lg-4">
							<?php
								if(get_post_thumbnail_id()):
									$thumb_id = get_post_thumbnail_id();
									$thumb_url = wp_get_attachment_image_src($thumb_id, 'blog_thumbnail', true);
									$thumb_url = $thumb_url[0];
								else:
									$thumb_url = '/wp-content/themes/abm-catering/img/no-blog-thumb.gif';
								endif;					
							?>
							<div class="inner" style="background-image: url('<?php echo $thumb_url; ?>') ;"> 
								<div class="details">
									<div class="date">
										<p><?php echo get_the_date('d '); ?><span><?php echo get_the_date('M'); ?></span></p>
									</div>
									<div class="desc">
										<p class="title"><a href="<?php the_permalink(); ?>"><?php if (strlen($post->post_title) > 40) : echo substr(the_title($before = '', $after = '', FALSE), 0, 40) . '...'; else: the_title(); endif; ?></a></p>
										<?php the_excerpt(); ?>
									</div>
								</div>
							</div>
						</article>
					<?php endwhile; ?>
				</div>
				
				<?php if( get_previous_posts_link() || get_next_posts_link()) : ?>
				<div class="navigation cf">
					<div class="newer"><?php previous_posts_link( 'Newer posts' ); ?></div>
					<div class="older"><?php next_posts_link( 'Older posts' ); ?></div>
				</div>
				<?php endif; ?>

			<?php endif; ?>
			*/ ?>


		</div>		
	</div>
	
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<div id="accreditation-wrap">
		<?php include(get_template_directory()."/page-templates/inc/accreditations.php"); ?>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>

</div>

<?php get_footer(); ?>