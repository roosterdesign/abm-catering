<?php get_header(); ?>
<div id="pageWrap">	
	<?php if (has_post_thumbnail( $post->ID ) ): $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog_hero' ); endif; ?>
	<div class="container-fluid" id="hero" style="background-image: url('<?php echo $image[0]; ?>');">
		<div class="strip">
			<div class="container">
				<div class="inner">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<div class="block cf">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="meta">
							<p>
								<?php echo get_the_date('l, jS F'); ?>
								<?php $categories = get_the_category(); $separator = ', '; $output = '';
									if ( ! empty( $categories ) ) : ?>
									<span>/</span> In 
									   <?php foreach( $categories as $category ) :
									        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
									    endforeach;
									    echo trim( $output, $separator );
								endif; ?>
								<span>/</span> By <?php the_author(); ?></p>
						</div>					
						<?php the_content(); ?>
						<div class="social-share">
							<?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_iytj"]'); ?>
						</div>
						<div class="navigation cf">
							<div class="newer"><?php previous_post_link('%link', 'Last post'); ?></div>
							<div class="older"><?php next_post_link('%link', 'Next post'); ?></div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
			<aside class="col-sm-5 col-md-4">
				<div class="inner">
				<h3>Recent Posts</h3>
				<?php global $post; $args = array( 'posts_per_page' => 3, 'post__not_in' => array( $post->ID ) ); $myposts = get_posts( $args ); ?>
						<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<article class="article">
							<?php echo get_avatar( get_the_author_meta( 'ID' ), 60 ); ?>							
							<p>
								<span class="date"><?php echo get_the_date('l, jS F'); ?></span>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</p>											
						</article>
						<?php endforeach; ?>			
					<?php wp_reset_postdata();?>
					<ul class="categories-list">
						<?php wp_list_categories(array('title_li' => '<h3>Categories</h3>', 'orderby' => 'term_order' )); ?>
					</ul>
				</div>
			</aside>
		</div>
	</div>
</div>
<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
<div id="accreditation-wrap">
	<?php include(get_template_directory()."/page-templates/inc/accreditations.php"); ?>
</div>
<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
<?php get_footer(); ?>