<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
        <!--[if lt IE 9]>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->  
		<?php global $trimmedAssetPath; wp_head(); ?>

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $trimmedAssetPath; ?>/img/meta/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $trimmedAssetPath; ?>/img/meta/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $trimmedAssetPath; ?>/img/meta/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $trimmedAssetPath; ?>/img/meta/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $trimmedAssetPath; ?>/img/meta/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $trimmedAssetPath; ?>/img/meta/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $trimmedAssetPath; ?>/img/meta/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<script src="https://use.typekit.net/qyl2kpu.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
	</head>


	<?php global $post;

	if(is_page(24) || $post->post_parent == 24 || is_page(354)):
		$bodyClass = 'honest';
	elseif(is_page(23) || $post->post_parent == 23 || is_page(357)):
		$bodyClass = 'mint';
	elseif(is_page(22) || $post->post_parent == 22 || is_page(359)):
		$bodyClass = 'essence';
	elseif(is_page(25) || $post->post_parent == 25 || is_page(358)):
		$bodyClass = 'venue';
	elseif(is_page(57) || $post->post_parent == 57 || is_page(355)):
		$bodyClass = 'grow';
	endif; ?>

<body <?php body_class($bodyClass); ?>>

	<header id="site-header">
		<div class="container">

		<div class="header-right cf">
			<ul>
				<li><a href="tel:01926498448"><i class="icon phone-sml"></i>01926 498 448</a></li>
				<li><a href="mailto:info@abmcatering.com"><i class="icon email-sml"></i>info@abmcatering.com</a></li>
			</ul>

			<ul class="social">
				<li><a href="https://www.facebook.com/abmcatering/?ref=bookmarks" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="https://twitter.com/abmcateringltd" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="https://www.linkedin.com/company-beta/11135841" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<li><a href="https://www.instagram.com/abmcatering/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
		
		<a href="/" class="logo">
			<?php if ( is_page_template('page-templates/homepage.php')): ?>
				<img src="<?php echo $trimmedAssetPath; ?>/img/header/home-logo.gif" alt="ABM Catering" height="110" width="120">
			<?php else: ?>
				<img src="<?php echo $trimmedAssetPath; ?>/img/header/logo.png" alt="ABM Catering" height="110" width="120">
			<?php endif; ?>
		</a>		
		
		<div class="nav-toggle"><span></span><span></span><span></span><span></span><span></span><span></span></div>
		<nav>
			<?php wp_nav_menu( array( 'menu' => 'Main Nav', 'container' => '' ) ); ?>
		</nav>

		</div>
	</header>