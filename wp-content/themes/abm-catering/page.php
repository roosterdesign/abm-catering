<?php get_header(); ?>
<div id="pageWrap">
	<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/child-subnav.php"); ?>
	<div class="main">
		<div class="container">
			<div class="block full cf">
				<div class="content">
					<div class="inner">
						<?php the_content() ; ?>						
					</div>
				</div>
			</div>			
		</div>
	</div>
	<?php include(get_template_directory()."/page-templates/inc/making-a-difference.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/latest-news.php"); ?>
	<?php include(get_template_directory()."/page-templates/inc/contact-cta.php"); ?>
</div>
<?php get_footer(); ?>