<?php get_header(); ?>


    <?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php //the_content(); ?>
    <?php //endwhile; endif; ?>

    <?php if (have_posts()) : ?>
        <h1>Search Results</h1>
        <?php 
            $total_results = $wp_query->found_posts; 
            echo '<p>We found ' . $total_results . ' results for the search "' . $s . '".</p>';
            while (have_posts()) : the_post(); 
        ?>
        <hr>
        <article <?php post_class() ?>>
            <h2 id="post-<?php the_ID(); ?>">
              <?php

              $theURL = get_permalink(); 
              $theURL = (string) $theURL;

              ?>
              <a href="<?php echo $theURL; ?>">
                <?php the_title(); ?>
              </a>
            </h2>

            <p>
              <?php 
            $caption = get_the_excerpt();
            echo substr($caption, 0,270);?> ...</p>

            <p><a href="<?php echo $theURL; ?>">Read More...</a></p>
          </article>
          <div class="c"></div>
        <?php endwhile; ?>

        <?php /* if (show_posts_nav()) : ?>
        <div class="b"></div>
        <nav id="pagination">
          <?php wp_pagenavi(); ?>
        </nav>
        <?php endif;*/ ?>

      <?php else : ?>
        <h1>Nothing found</h1>
        <p>Sorry, nothing was found that matched your search. Why not try a different search?</p>
      <?php endif; ?>

<?php get_footer(); ?>